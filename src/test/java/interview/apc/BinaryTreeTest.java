package interview.apc;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static interview.apc.BinaryTree.NO;
import static interview.apc.BinaryTree.YES;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BinaryTreeTest {

    private BinaryTree tree = new BinaryTree();

    @Test
    public void testEdgeLength() {
        assertEquals(NO, tree.solution(Arrays.asList("aaa")));
    }

    @Test
    public void testAlphaEdge() {
        assertEquals(NO, tree.solution(Arrays.asList("1a")));
    }

    @Test
    public void testSameNode() {
        assertEquals(NO, tree.solution(Arrays.asList("aa")));
    }

    @Test
    public void testMoreChildren() {
        assertEquals(NO, tree.solution(Arrays.asList("ab", "ac", "ad")));
    }

    @Test
    public void testCycle() {
        assertEquals(NO, tree.solution(Arrays.asList("ab", "bc", "ca")));
    }

    @Test
    public void testCrossEdge() {
        assertEquals(NO, tree.solution(Arrays.asList("ab", "ac", "bc")));
    }

    @Test
    public void testOneTreePlusOneCycle() {
        assertEquals(NO, tree.solution(Arrays.asList("ab", "ac", "de", "ed")));
    }

    @Test
    public void testTwoTrees() {
        assertEquals(NO, tree.solution(Arrays.asList("ab", "cd")));
    }

    @Test
    public void testTree() {
        assertEquals(YES, tree.solution(Arrays.asList("ab", "bf", "ac", "be", "cn")));
    }
}
