package interview.apc;


import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChipsetFactoryTest {


    @Test
    public void testNoSolution() {
        ChipsetFactory factory = new ChipsetFactory();
        assertEquals(set(asList()), factory.solution(new int[]{4, 5, 6}, 3));
        assertEquals(3, factory.getWaste());
    }

    @Test
    public void testSimpleSolution() {
        ChipsetFactory factory = new ChipsetFactory();
        assertEquals(set(asList(1, 2), asList(3)), factory.solution(new int[]{1, 2, 3}, 3));
        assertEquals(0, factory.getWaste());
    }

    @Test
    public void testExtraWaste() {
        ChipsetFactory factory = new ChipsetFactory();
        assertEquals(set(asList(3, 4)), factory.solution(new int[]{2, 3, 4}, 8));
        assertEquals(1, factory.getWaste());
    }

    @Test
    public void testCurrentSolution() {
        ChipsetFactory factory = new ChipsetFactory();
        assertEquals(set(asList(1, 4, 6), asList(2, 4, 5), asList(1, 10), asList(5, 6)), factory.solution(new int[]{1, 2, 4, 10, 5, 6}, 11));
        assertEquals(0, factory.getWaste());
    }


    private static Set<List<Integer>> set(List<Integer>... elems) {
        Set<List<Integer>> res = new HashSet<>();
        for (List<Integer> e : elems) {
            res.add(e);
        }
        return res;
    }
}
