package interview.apc;


import java.util.*;
import java.util.stream.Collectors;

public class BinaryTree {

    public static final String NO = "NO", YES = "YES";

    public static void main(String[] args) {
        System.out.println(new BinaryTree().solution(Arrays.asList("ab", "bf", "ac", "be", "cn")));
    }

    private boolean isAlpha(char c) {
        return 'a' <= c && c <= 'z';
    }

    /**
     * Method returning YES or NO depending on whether the edges form a binary tree or not.
     *
     * @param input contains all the edges
     * @return YES if it's a binary tree and NO otherwise
     */
    public String solution(List<String> input) {
        Collection<String> edges = new HashSet<>(input); // get uniques
        // validate edges
        for (String edge : edges) {
            char c1 = edge.charAt(0);
            char c2 = edge.charAt(1);
            if (edge.length() != 2 || c1 == c2 || !isAlpha(c1) || !isAlpha(c2)) {
                return NO;
            }
        }
        // check for parents and children
        // a binary tree has one root (no parent) and nodes with max 2 children
        Map<Character, Set<Character>> parents = new HashMap<>();
        Map<Character, Set<Character>> children = new HashMap<>();

        for (String edge : edges) {
            char c1 = edge.charAt(0);
            char c2 = edge.charAt(1);

            parents.computeIfAbsent(c1, key -> new HashSet<>()); // empty for parent
            parents.computeIfAbsent(c2, key -> new HashSet<>()).add(c1); // c1 is parent of c2

            children.computeIfAbsent(c1, key -> new HashSet<>()).add(c2); // c2 is child of c1
            children.computeIfAbsent(c2, key -> new HashSet<>()); // no children

            // validate for more than 1 parent - takes care of cross edges - we can still have cycles
            if (parents.get(c2).size() > 1) return NO;

            // validate for more than 2 children - makes sure it's a binary tree
            if (children.get(c1).size() > 2) return NO;
        }

        // find root (should be unique)
        List<Character> roots = parents.entrySet().stream().filter(e -> e.getValue().size() == 0)
                .map(e -> e.getKey()).collect(Collectors.toList());

        if (roots.size() != 1) return NO; // we can have multiple trees

        // perform dfs to make sure that all the nodes are reached from the root
        // if that's not the case then we have a cycle not connected to this tree
        Set<Character> visited = new HashSet<>();
        dfs(roots.get(0), children, visited);


        return parents.size() == visited.size() ? YES : NO;
    }

    private void dfs(Character node, Map<Character, Set<Character>> children, Set<Character> visited) {
        visited.add(node);

        for (char child : children.get(node)) {
            if (!visited.contains(child)) {
                dfs(child, children, visited);
            }
        }
    }


}
