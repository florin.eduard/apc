package interview.apc;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ChipsetFactory {

    private int waste = Integer.MAX_VALUE;

    public static void main(String[] args) throws IOException {
        int[] nums;
        int k;
        try (FileInputStream fis = new FileInputStream(ChipsetFactory.class.getResource("/interview/apc/input.txt").getFile());
             InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(isr)
        ) {
            int n = Integer.valueOf(reader.readLine()); // read first line
            nums = new int[n];
            int i = 0;
            String[] splits = reader.readLine().split(" "); // read numbers
            while (i < n) {
                nums[i] = Integer.valueOf(splits[i]);
                i++;
            }
            k = Integer.valueOf(reader.readLine()); // read target
        }
        ChipsetFactory factory = new ChipsetFactory();

        Set<List<Integer>> res = factory.solution(nums, k);

        try (FileWriter writer = new FileWriter("target/classes/output.txt")) {
            writer.write("Nr solutions=" + res.size() + "\n");
            for (List<Integer> list : res) {
                writer.write(list.toString() + "\n");
            }
            writer.write("Waste=" + factory.waste);
        }
    }

    public Set<List<Integer>> solution(int[] nums, int k) {
        Set<List<Integer>> res = new HashSet<List<Integer>>();
        backtrack(nums, 0, k, new ArrayList<Integer>(), res);
        return res;
    }


    public int getWaste() {
        return waste;
    }

    /**
     *  Backtrack by checking all the possibilities. For every machine there are two options, either
     *  use the machine or not use it.
     *  End condition: all the machines have been considered or the target k < 0.
     * @param nums
     * @param start current index in nums
     * @param k target value
     * @param list working list
     * @param res set of lists
     */
    private void backtrack(int[] nums, int start, int k, List<Integer> list, Set<List<Integer>> res) {
        if(k < 0) return;

        if (start == nums.length) {
            if (waste > k) { // if we identify a case with smaller waste, start fresh
                waste = k;
                res.clear();
            }
            if(waste == k) {
                res.add(new ArrayList<Integer>(list));
            }
            return;
        }

        // two options -> take or not take
        // 1
        list.add(nums[start]);
        backtrack(nums, start + 1, k - nums[start], list, res);
        list.remove(list.size() - 1); // pop last
        // 2
        backtrack(nums, start + 1, k, list, res);
    }

}
